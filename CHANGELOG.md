## [Unreleased]

## [0.2.2] - 2023-10-19

- Upgrade to Natsukantou 0.2.2
  - Remove newline characters in HandleRubyMarkup, which helps improving translation
  - Fix ChatGPT not able to distinguish content to be translated and our instructions.

## [0.2.0] - 2023-03-05

- Add ChatGPT translator as experiment

## [0.1.1] - 2023-02-27

- Fix crash if epub document does not have `lang` attribute
- Skip interlace on nodes if they don't differ
- 
## [0.1.0] - 2022-07-02

- Initial release
