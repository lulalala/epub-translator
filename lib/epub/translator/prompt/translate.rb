# frozen_string_literal: true

require 'tty-prompt'
require 'rake/ext/string'
require 'natsukantou/setup/config_load_or_prompt'
require 'fileutils'

module Epub
  module Translator
    module Prompt
      class Translate
        attr_reader :prompt

        def execute(epub_path:, config_path: nil, prompt: ::TTY::Prompt.new)
          begin
            EPUB::Parser.parse(epub_path)
          rescue Errno::ENOENT
            warn 'Epub not found'
            return
          rescue Archive::Zip::UnzipError
            warn 'Not an Epub'
            return
          end

          translator = Natsukantou::Setup::ConfigLoadOrPrompt.new.execute(
            prompt: prompt,
            config_path: config_path,
          )

          lang_from = prompt.ask("Language code to translate from?")
          lang_from = Natsukantou::LanguageCode.new(lang_from)
          lang_to = prompt.ask("Language code to translate to?")
          lang_to = Natsukantou::LanguageCode.new(lang_to)

          # Make a copy for modification

          new_epub_path = prompt.ask(
            'Where do you want to save the result?',
            value: epub_path.pathmap("%X.#{lang_to.code}%x")
          )
          FileUtils.copy(epub_path, new_epub_path)

          book = EPUB::Parser.parse(new_epub_path)

          items = book.each_page_on_spine.select(&:xhtml?)
          items_untranslated_index = items.map.with_index do |item, index|
            if (doc_lang_code = item.content_document.oga.lang)
              lang = Natsukantou::LanguageCode.new(doc_lang_code)
              lang.is?(lang_to) ? nil : index
            else
              index
            end
          end.compact

          choices = items.map.with_index do |item, index|
            translated = !items_untranslated_index.include?(index)
            [item_label(item, translated), item]
          end.to_h

          targets = prompt.multi_select(
            "Select items to translate:",
            choices,
            default: items_untranslated_index.map { |index| index + 1 },
            per_page: 30, echo: false
          )

          targets.each do |item|
            output_translation_progress(item)

            env = Natsukantou::Env.new(
              dom: item.content_document.oga,
              lang_from: lang_from,
              lang_to: lang_to
            )
            translator.call(env)

            item.content = env[:dom].to_xml
            item.save
          end

          if !targets.empty?
            puts 'Translation complete.'
          end
        end

        private

        def output_translation_progress(item)
          puts item.content_document.title
        end

        def item_label(item, translated)
          text = "#{item.content_document.title} (#{item.id})"

          if translated
            text += " (already in #{item.content_document.oga.lang})"
          end

          text
        end
      end
    end
  end
end
