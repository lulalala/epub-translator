# frozen_string_literal: true

require 'epub/maker'
require 'oga'
require 'fileutils'
require 'rake/ext/string'
require 'tty-prompt'

module Epub
  module Translator
    module Prompt
      class Interlace
        attr_reader :epub_paths, :interlace_level, :prompt

        def execute(
          epub_paths:,
          prompt: ::TTY::Prompt.new
        )
          @epub_paths = epub_paths
          @prompt = prompt

          epub_paths.each do |path|
            return unless valid_epub?(path) # rubocop:disable Lint/NonLocalExitFromIterator
          end

          new_epub_path = prompt.ask(
            'Where do you want to save the result?',
            value: epub_paths.last.pathmap("%X.interlaced%x")
          )

          @interlace_level = prompt.ask(
            "Please input CSS path of DOM level to interlace:",
            value: 'body p,h1,h2,h3,h4,h5,h6'
          )

          FileUtils.copy(epub_paths[0], new_epub_path)

          epub = EPUB::Parser.parse(new_epub_path)
          epub_other = EPUB::Parser.parse(epub_paths[1])

          epub.each_page_on_spine.select(&:xhtml?).each do |item|
            next if item.content_document.oga.css(interlace_level).empty?

            item_other = epub_other.manifest[item.id]
            lang_other = item_other.content_document.oga.lang

            new_dom = item.content_document.oga
            new_dom.interlace(
              item_other.content_document.oga,
              level: interlace_level
            )

            # Style
            new_dom.lang = nil
            style = Oga::XML::Element.new(name: 'style')
            style.inner_text = "*:lang(#{lang_other}) { color: #999 !IMPORTANT; }"
            new_dom.at_css('head').node_set.push(style)

            item.content = new_dom.to_xml
          end
          epub.save

          puts 'Interlacing complete.'
        end

        private

        def valid_epub?(path)
          EPUB::Parser.parse(path)
          true
        rescue Errno::ENOENT
          warn 'Epub not found'
          false
        rescue Archive::Zip::UnzipError
          warn 'Not an Epub'
          false
        end
      end
    end
  end
end
