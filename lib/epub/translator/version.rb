# frozen_string_literal: true

module Epub
  module Translator
    VERSION = "0.2.2"
  end
end
