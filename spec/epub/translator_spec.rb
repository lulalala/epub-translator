# frozen_string_literal: true

RSpec.describe Epub::Translator do
  it "has a version number" do
    expect(Epub::Translator::VERSION).not_to be nil
  end
end
