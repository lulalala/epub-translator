# frozen_string_literal: true

require 'rspec'
require "epub/translator/prompt/translate"
require "tty/prompt/test"

# See https://github.com/piotrmurach/tty-prompt/issues/148
class StringIO
  def ioctl(*)
    80
  end
end

RSpec.describe Epub::Translator::Prompt::Translate do
  let(:translator) { proc { |env| env } }
  let(:prompt) { TTY::Prompt::Test.new }
  let(:lang_from) { 'en' }
  let(:lang_to) { 'fr' }
  let(:epub_path) { "./spec/fixtures/khalil-gibran_the-madman.epub" }
  let(:new_epub_path) { "./spec/fixtures/khalil-gibran_the-madman.#{lang_to}.epub" }

  before do
    allow(Natsukantou::Setup::ConfigLoadOrPrompt).
      to receive_message_chain(:new, :execute).
      and_return(translator)
  end

  after do
    File.delete(new_epub_path) if File.exist?(new_epub_path)
  end

  context 'when defaults are used' do
    it "creates new epub file" do
      prompt.input << "#{lang_from}\n"
      prompt.input << "#{lang_to}\n"
      prompt.input << "\n" # new file path default
      prompt.input << "\n" # select items default
      prompt.input.rewind

      expect do
        subject.execute(
          prompt: prompt,
          epub_path: epub_path
        )
      end.to output(include('Prologue', 'Uncopyright', 'Translation complete')).to_stdout

      expect(File.exist?(new_epub_path)).to eq(true)
    end
  end

  context 'when items are deselected' do
    it "creates new epub file" do
      prompt.input << "#{lang_from}\n"
      prompt.input << "#{lang_to}\n"
      prompt.input << "\n" # new file path default
      prompt.input << " \n" # select items deselect last item
      prompt.input.rewind

      expect do
        subject.execute(
          prompt: prompt,
          epub_path: epub_path
        )
      end.not_to output(include('Uncopyright')).to_stdout

      expect(File.exist?(new_epub_path)).to eq(true)
    end
  end

  context 'when file is not zip' do
    let(:epub_path) { "./README.md" }

    it "exits" do
      expect do
        subject.execute(
          prompt: prompt,
          epub_path: epub_path
        )
      end.to output(include('Not an Epub')).to_stderr
    end
  end

  context 'when document does not have lang attribute' do
    let(:epub_path) { "./spec/fixtures/demo.epub" }
    let(:new_epub_path) { "./spec/fixtures/demo.#{lang_to}.epub" }

    it "treats document as untranslated" do
      prompt.input << "#{lang_from}\n"
      prompt.input << "#{lang_to}\n"
      prompt.input << "\n" # new file path default
      prompt.input << "\n" # select items default
      prompt.input.rewind

      expect do
        subject.execute(
          prompt: prompt,
          epub_path: epub_path
        )
      end.to output(include('Essay', 'Translation complete')).to_stdout

      expect(File.exist?(new_epub_path)).to eq(true)
    end
  end
end
