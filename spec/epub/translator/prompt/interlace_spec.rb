# frozen_string_literal: true

require 'rspec'
require "epub/translator/prompt/interlace"
require "tty/prompt/test"
require 'debug'
require 'pry'

# See https://github.com/piotrmurach/tty-prompt/issues/148
class StringIO
  def ioctl(*)
    80
  end
end

RSpec.describe Epub::Translator::Prompt::Interlace do
  let(:translator) { proc { |env| env } }
  let(:prompt) { TTY::Prompt::Test.new }
  let(:epub_path) { "./spec/fixtures/khalil-gibran_the-madman.epub" }
  let(:epub_path_other) { "./spec/fixtures/khalil-gibran_the-madman.ja.epub" }
  let(:new_epub_path) { "./spec/fixtures/khalil-gibran_the-madman.ja.interlaced.epub" }

  after do
    File.delete(new_epub_path) if File.exist?(new_epub_path)
  end

  context 'when items are deselected' do
    it "creates new epub file" do
      prompt.input << "\n" # new file path default
      prompt.input << "\n" # interlacing default
      prompt.input.rewind

      expect do
        subject.execute(
          prompt: prompt,
          epub_paths: [epub_path, epub_path_other]
        )
      end.to output(include('Interlacing complete')).to_stdout

      expect(File.exist?(new_epub_path)).to eq(true)
    end
  end
end
