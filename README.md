# Epub::Translator

Translate Epub file with machine translation services such as DeepL and ChatGPT.

## Installation

Install the gem and add to the application's Gemfile by executing:

    $ bundle add epub-translator

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install epub-translator

## Usage

To run it as a command:

    $ epub-translator [EPUB_FILE]

It's a wizard that guides you through setting up a translator configuration...

![Wizard](./docs/eg_wizard_config.png)

and then asks you how and what to translate. Enter the language codes (e.g. "en"),
and select chapters to translate (by default all are selected).

![Wizard](./docs/eg_wizard_translate.png)

During the process, the wizard will ask you whether you want to save the configuration for later reuse.
If you choose yes, the config will be saved as `translator_config.rb` file.

You will be able to use this config later by using the `-c` flag:

    $ epub-translator [EPUB_FILE] -c translator_config.rb

## Interlacing

After you have the translated file, you can choose to interlace it against the original by running:

    $ epub-interlace [EPUB_FILE_1] [EPUB_FILE_2]

It will generate another EPUB file, with translated paragraphs next to their sources.

![Interlaced result](./docs/eg_interlace.png)

## Feature

Epub::Translator uses [Natsukantou](https://gitlab.com/lulalala/natsukantou) gem for translating XMLs. It uses the middleware pattern to provide customizability, e.g. you can decide whether to flatten HTML Ruby Markup. A wizard will guide you through the configuration process.

[List of supported translation services](https://gitlab.com/lulalala/natsukantou#translators)

[List of middlewares (filters)](https://gitlab.com/lulalala/natsukantou#middlewares)

## Development

The XML related operations are purposefully extracted into its own gem: [Natsukantou](https://gitlab.com/lulalala/natsukantou).

This gem uses [Oga XML parser](https://gitlab.com/yorickpeterse/oga) and [epub-parser](https://gitlab.com/KitaitiMakoto/epub-parser) (both hosted on GitLab, yay!).

The wizard process uses [tty-prompt](https://github.com/piotrmurach/tty-prompt) and [yard](https://github.com/lsegal/yard).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/lulalala/epub-translator.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

The EPUB included in the repository is [The Madman by Khalil Gibran](https://standardebooks.org/ebooks/khalil-gibran/the-madman), which is licensed under [public domain](https://creativecommons.org/publicdomain/zero/1.0/). I use it for testing purposes because it is a very short book.
