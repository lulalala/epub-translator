# frozen_string_literal: true

require_relative "lib/epub/translator/version"

Gem::Specification.new do |spec|
  spec.name = "epub-translator"
  spec.version = Epub::Translator::VERSION
  spec.authors = ["lulalala"]
  spec.email = ["mark@goodlife.tw"]

  spec.summary = "Tools to translate EPUB books"
  spec.homepage = "http://gitlab.com/lulalala/epub-translator"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.7.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "epub-maker", "~> 0.1.7"
  spec.add_dependency "epub-parser", "~> 0.4.6"
  spec.add_dependency "middleware", "~> 0.1.0"
  spec.add_dependency "natsukantou", "~> 0.2.2"
  spec.add_dependency "tty-prompt", "~> 0.23"

  spec.add_development_dependency "debug", ">= 1.0.0"
  spec.add_development_dependency "pry"
  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
